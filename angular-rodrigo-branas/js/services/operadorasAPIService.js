angular.module("listaTelefonica").factory("operadorasAPI", function($http, config) {

    var _getOperadoras = function() {
        //return $http.get("http://localhost:3412/operadoras");
        return $http.get(config.baseUrl + "/operadoras");
    };

    var _saveOperadora = function(operadora) {
        return $http.post(config.baseUrl + "/contatos", operadora);
    };

    return {
        getOperadoras: _getOperadoras,
        saveOperadora: _saveOperadora
    };
});
