angular.module("listaTelefonica", ["ngMessages"]);
angular.module("listaTelefonica").controller("listaTelefonicaController", function($scope, contatosAPI, operadorasAPI) {
    $scope.app = "Lista Telefonica";
    // $scope.contatos = [
    //         {nome: "Vitor", telefone: "998212157", operadora: {nome: "Oi", codigo: 31}, data: new Date(), cor: "#6d6db7"},
    //         {nome: "Sérgio", telefone: "998212101", operadora: {nome: "Vivo", codigo: 45}, data: new Date(), cor: "#926db7"},
    //         {nome: "Maria", telefone: "998211548", operadora: {nome: "Tim", codigo: 41}, data: new Date(), cor: "#ac8fc8"}
    // ];
    $scope.contatos = [];

    // $scope.operadoras = [
    //         {nome: "Vivo", codigo: 45, categoria: "Celular", preco: 2},
    //         {nome: "Oi", codigo: 31, categoria: "Celular", preco: 2},
    //         {nome: "Tim", codigo: 41, categoria: "Celular", preco: 1},
    //         {nome: "Claro", codigo: 21, categoria: "Celular", preco: 1},
    //         {nome: "Embratel", codigo: 21, categoria: "Fixo", preco: 3},
    //         {nome: "GVT", codigo: 21, categoria: "Fixo", preco: 3}
    // ];
    $scope.operadoras = [];
    $scope.contato = {
        data: 1034218800000
    };

    var carregarContatos = function() {
        contatosAPI.getContatos()
            .success(function(data, status) {
                $scope.contatos = data;
            })
            .error(function(data) {
                $scope.error = "Aconteceu um erro!: " + data;
            });
    };

    var carregarOperadoras = function() {
        operadorasAPI.getOperadoras()
            .success(function(data, status) {
                $scope.operadoras = data;
            })
            .error(function(data) {
                $scope.error = "Aconteceu um erro!: " + data;
            });
    };

    $scope.adicionarContato = function(contato) {
        contato.data = new Date();
        //console.log(contato);
        //$scope.contatos.push(angular.copy(contato));
        //$http.post("http://localhost:3412/contatos", contato)
        contatosAPI.saveContato(contato)
            .success(function(data) {
                delete $scope.contato; //Remove os dados da caixa após adicionado
                $scope.contatoForm.$setPristine(); //Muda o estado da caixa como "intocada"
                carregarContatos();
            });
    };

    $scope.apagarContatos = function(contatos) {
        $scope.contatos = contatos.filter(function(contato) {
            if (!contato.selecionado) return contato
        });
    };

    $scope.isContatoSelecionado = function(contatos) {
        return contatos.some(function(contato) {
            return contato.selecionado;
        });
    };

    $scope.ordernarPor = function(campo) {
        $scope.criterioDeOrdenacao = campo;
        $scope.direcaoDaOrdenacao = !$scope.direcaoDaOrdenacao;
    };

    carregarContatos();
    carregarOperadoras();
});
